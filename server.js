const express = require('express')
const logger = require('./logger')

const router = require('./router')

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use('/admins', router)

const PORT = process.env.PORT || 3000

app.listen(PORT,()=>logger.info(`Server started listening on PORT : ${PORT}`));
