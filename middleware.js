const jwt = require('jsonwebtoken')
const logger = require("./logger")

const authMiddleware = (request,respone,next) => {
    try {
        let jwtToken = null
        const authorizationHeader = request.headers["authorization"]
        if (authorizationHeader !== undefined) {
            jwtToken = authorizationHeader.split(" ")[1]
        } else {
            respone.status(401).send("Unauthorized User")
        }

        if (jwtToken !== undefined) {
            jwt.verify(jwtToken,process.env.SECRET_TOKEN, (error) => {
                if (error) {
                    respone.status(403).send("Invalid Access Token.")
                    logger.error("Invalid Access Token.")
                } else {
                    next()
                }
            })
        }
    } catch (error) {
        respone.status(400).send(error.message)
    }

}

module.exports = authMiddleware;
