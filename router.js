const dotenv = require('dotenv')
const express = require('express')
const router = express.Router()

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const logger = require('./logger.js')

const dbConnection = require('./database')
const authenticationMiddleware = require('./middleware.js')

dotenv.config()


//Sign Up or Register API

router.post('/signup', async (request,response) => {
    try {
        const isValidUsername = (username) => {
            const regex = /^[a-zA-Z ]{2,30}$/;
            return regex.test(username)
        }
        const isValidEmail = (email) => {
            const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
            return regex.test(email)
        }
        const isValidPassword = (password) => {
            return  ((password.length >= 5) && (password.length <= 16)) 
        }


        if (!isValidUsername(request.body.username)){
            response.status(401).send("Invalid user")
        }
        else if (!isValidEmail(request.body.email)){
            response.status(401).send("Invalid email")
        }
        else if (!isValidMObileNumber(request.body.mobile_no)){
            response.status(401).send("Invalid mobile number")
        }
        else if (!isValidPassword(request.body.password)){
            response.status(401).send("Characters in the password must be lies between 6 to 256")
        }
        else{
            const hashedPassword = await bcrypt.hash(password,10)
            const sqlQuery = `SELECT name FROM admins where email = "${request.body.email}";`

            dbConnection.query(sqlQuery,(error,data)=>{
                if (error) {logger.error(error.message)}
                if (data.length === 0) {
                    const sql = `INSERT INTO admins (name,email,mobile_no,password) values ("${name}","${email}","${mobile_no}","${hashedPassword}")`
                    dbConnection.query(sql,(error) => {
                        if (error) logger.error(error.message)
                        response.status(201).send(`Admin with NAME: ${name} and EMAIL: ${email} is created successfully.`)
                    })
                }
            })
        }
    } catch (error) {
        response.status(400).send(error.message)
    }
})


     



//Login API

router.post('/login',async (request,response) => {
    try {
        const {email,password} = request.body
        const sqlQuery = `SELECT * FROM admins where email="${email}";`
        
        dbConnection.query(sqlQuery, async (error, data)=>{
            if (error) logger.error(error.message)
            if (data.length !== 0) {
                const payload = {
                    email:email
                }
                const isPasswordMatched = await bcrypt.compare(password, data[0].password)
                if (isPasswordMatched) {
                    const jwtToken = generateAccessToken(payload)
                    const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN)
                    dbConnection.query(`INSERT INTO refreshtokens VALUES ("${email}","${refreshToken}")`)
                    response.status(201).json({"JWT_TOKEN": jwtToken,"REFRESH_TOKEN":refreshToken})
                } else {
                    response.status(401).send("Invalid Password")
                }
            } else {
                response.status(404).send("Not Found")
            }
        })
    } catch (error) {
        response.status(400).send(error.message)
    }
})

function generateAccessToken(payload) {
    return jwt.sign(payload, process.env.SECRET_TOKEN,{expiresIn:"2m"})
}

//API to generate new access token

router.post('/token',(request,response)=>{
    try {
        const refreshToken = request.body.token
        if (refreshToken === null) {
            logger.error("Unauthorized User")
            response.status(401).send("Unauthorized user")
        } else {
            dbConnection.query(`SELECT email from refreshtokens where token = "${refreshToken}";`,(error,data) => {
                if (error) logger.error(error.message)
                if (data.length!==0) {
                    jwt.verify(refreshToken,process.env.REFRESH_TOKEN, (error,payload)=>{
                        if (error) {
                            return response.status(403).send("Invalid Refresh Token")
                        }
                        const accessToken = generateAccessToken({email:payload.email})
                        logger.info("New Access Token Generated.")
                        response.send({accessToken})
                    })
                } else {
                    response.status(404).send("Refresh token is not found in the database.")
                }
                
            })
        }
    } catch (error) {
        response.status(400).send(error.message)
    }
});

//API to read the data from the database

router.get('/employees',authenticationMiddleware,(request,response) => {
    try {
        const sql = "SELECT * FROM employees"
        dbConnection.query(sql,(error,data)=>{
            (error) ? logger.error(error.message) : response.status(200).send(data)
        })
    } catch (error) {
        response.status(400).send(error.message)
    }
});

//API to update the data from the database

router.put('/employees/:id', authenticationMiddleware, (request,response) => {
    try {
        const {name,designation,company_name,email,mobile_no,address,company_location} = request.body
        const sqlQuery = `SELECT * FROM employees where id = ${request.params.id};`
        dbConnection.query(sqlQuery,(error,data) => {
            if (error) logger.error(error.message)
            if (data.length === 0) {
                response.status(404).send(`Data is not found with this ID: ${request.params.id}`)
            } else {
                const sql = `UPDATE employees
                    SET 
                        name= "${name}",
                        designation="${designation}",
                        company_name="${company_name}",
                        email="${email}",
                        mobile_no = "${mobile_no}",
                        address="${address}",
                        company_location="${company_location}"
                    where id = ${request.params.id};`
                dbConnection.query(sql,(error) => {
                    (error) ? logger.error(error.message) : response.status(200).send(`Employee with ID: ${request.params.id} is updated successfully.`)
                })
            }
        })
    } catch (error) {
        response.status(400).send(error.message)
    }
})

//API to delete the data from the database

router.delete('/employees/delete/:id', authenticationMiddleware, (request,response) => {
    try {
        const sqlQuery = `SELECT * FROM employees where id = ${request.params.id};`
        dbConnection.query(sqlQuery,(error,data) => {
            if (error) logger.error(error.message)
            if (data.length === 0) {
                response.status(404).send(`Data is not found with this ID: ${request.params.id}`)
            } else {
                logger.info("Working")
                const sql = `DELETE FROM employees
                    where id = ${request.params.id};`
                dbConnection.query(sql,(error) => {
                    (error) ? logger.error(error.message) : response.status(200).send(`Employee with ID: ${request.params.id} is deleted.`)
                })
            }
        })

    } catch (error) {
        response.status(400).send(error.message)
    }
});

//API to logout from the application

router.delete('/logout', authenticationMiddleware, (request,response) => {
    try {
        const sqlQuery = `DELETE FROM refreshtokens;`
        dbConnection.query(sqlQuery, (error) => {
            (error) ? logger.error(error.message) : response.status(200).send("Logout Successfull") 
        })
    } catch (error) {
        response.status(400).send(error.message)
    }
})

module.exports = router;
