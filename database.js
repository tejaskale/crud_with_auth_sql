const mysql = require('mysql');
const logger = require('./logger');

const dbConnection = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"tejas@22",
    database: "empDatabase"
});

dbConnection.connect((error)=>{
    if (error) return logger.error(error.message)
    logger.info("Database connected successfully.") 
});

module.exports = dbConnection;
